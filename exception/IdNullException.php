<?php
namespace BWB\Framework\mvc\exception;

use Exception;

class IdNullException extends Exception
{
   public function __construct($message)
   {
       parent::__construct($message);
       if(is_null($message)){
        $this->message = "Error: Id's null, int expected";
       }
   } 
}