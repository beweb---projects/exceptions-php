<?php
namespace BWB\Framework\mvc\exception;

use Exception;

class BadQueryException extends Exception
{
    public function __construct($message)
   {
       parent::__construct($message);
       $this->message = $message;
   } 
}